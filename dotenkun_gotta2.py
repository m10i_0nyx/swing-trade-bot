#!/usr/bin/python3.6 -tt
# -*- coding: utf-8 -*-

# Copyright (c) 2018 m10i <m10i@0nyx.net>
#
#----------------------------------------------------------
# *** 利用ライセンスについて ***
# 下記の条件を受け入れていただけるのであれば、誰でも自由に無料で、このソフトウェアをつかっていただくことができます
# ・このソースコードには一切の保証は有りません
# ・このソースコードを利用したことで何らかの問題・損害を負ったとしても、作者はなんの責任も負いません
# ・ソースコードを自身の利用範囲でコピーしたり、変更を加えるのは自由です
# ・ただし作者の許可無しでの第3者への再配布および販売は固く禁じます
# ・この条件を逸脱した場合は作者(もしくは関係者)より利用禁止・販売停止、および損害賠償を要求いたします
#----------------------------------------------------------
#
# *** 必要ライブラリ ***
# pip3 install --user requests pybitflyer ccxt pytz pubnub

import argparse
import requests
from requests.exceptions import ConnectionError, HTTPError
import datetime
import os
import importlib
import time
from logging import getLogger, Formatter as LogFormatter, StreamHandler as LogStreamHandler, DEBUG as LOG_DEBUG, INFO as LOG_INFO
import logging.handlers
from pytz import timezone, utc
from urllib.error import URLError, HTTPError
import json
import traceback

parser = argparse.ArgumentParser(
    description='This script is a program that BTC-FX/JPY bot.')

parser.add_argument('-C', '--config-file',
                    action='store',
                    default='dotenkun_config.json',
                    type=str,
                    help='設定ファイル名 (default: dotenkun_config.json)')

parser.add_argument('--timezone',
                    action='store',
                    default='Asia/Tokyo',
                    type=str,
                    help='Timezone (default: Asia/Tokyo)')

parser.add_argument('--debug',
                    action='store_true',
                    default=False,
                    help='デバッグレベルのコンソールログ出力許可 (default: False)')

parser.add_argument('--disable-log-file',
                    action='store_true',
                    default=False,
                    help='ログファイルへの出力停止 (default: False)')

parser.add_argument('--log-file-name',
                    action='store',
                    default='logs/dotenkun_log.txt',
                    type=str,
                    help='ログファイル名 (default: logs/dotenkun_log.txt)')

parser.add_argument('--dry-run',
                    action='store_true',
                    default=False,
                    help='テストモード(注文停止) (default: False)')

args = parser.parse_args()

TIMEZONE_LOCAL = timezone(args.timezone)

#---------------------------------------------------------------------------

# LINE Notify用関数
def line_notify(message, api_token):
    headers = {
        'Authorization':'Bearer ' + api_token
    }
    payload = {
        'message':"\n" + message
        # 日時付き
        #'message':"\n" + message + "\n" + datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")
    }

    api_url   = 'https://notify-api.line.me/api/notify'
    response = requests.post(api_url ,headers=headers ,params=payload)
    if response.status_code == 200:
        logger.debug('LINE Notify 送信成功')
    elif response.status_code == 401:
        logger.warning('LINE Notify 認証失敗(Token誤り?)')
    else:
        logger.warning(response)

LTP_LAST = 0
def output_indicator(last, c_high, c_low, side, size):
    global last_last
    # last = o['last']
    # c_high = o['c_high']
    # c_low = o['c_low']
    # pos = o['pos']
    acceleration = 1 if LTP_LAST is 0 else last / LTP_LAST  #または o['ohlcv']['c'][-1] / o['ohlcv']['c'][-2]
    leftStr = ' '
    rightStr = ' '

    dp_ratio = (last-c_low)/(c_high-c_low)
    overload = 1 if dp_ratio > 1 else -1 if dp_ratio < 0 else 0  # check the overload

    if overload == 0:
        dpos = min(19, round(dp_ratio*20))  # max is 19
        dpos = max(0,dpos)                  # min is 0
        indicator = ' ['
        for i in range(0, dpos):
            indicator += '-'
        indicator += '*'
        for i in range(0, 19-dpos):
            indicator += '-'
        indicator += '] '
    elif overload == 1:
        indicator = ' [--------------------]*'
        if side == 'SHORT' or side is None:
            rightStr = 'BUY'
    elif overload == -1:
        indicator = '*[--------------------] '
        if side == 'LONG' or side is None:
            leftStr = 'SELL'

    return '{0:^6} {1:>.1f} '.format(leftStr, c_low) + indicator + ' {0:<.1f} {1:^6}'.format(c_high, rightStr)

#---------------------------------------------------------------------------
# 下記よりメイン処理

logger = getLogger(__name__)
logger.setLevel(LOG_DEBUG)

handler_console = LogStreamHandler()
if args.debug == True:
    handler_console.setLevel(LOG_DEBUG)
else:
    handler_console.setLevel(LOG_INFO)
#handler_console.setFormatter(LogFormatter('[%(asctime)s][%(funcName)s:%(lineno) 3d][%(levelname)-8s] %(message)s'))
handler_console.setFormatter(LogFormatter('[%(asctime)s][%(levelname)-8s] %(message)s'))
logger.addHandler(handler_console)

# ログファイル出力設定
if args.disable_log_file is False and args.log_file_name is not None:
    if not os.path.exists('./logs'):
        os.mkdir('./logs')
    handler_file = logging.handlers.TimedRotatingFileHandler(
        filename = args.log_file_name,
        when = 'D', #一日ごとにログローテーション
        backupCount=60, #60日分保持
        encoding='utf-8'
    )
    logger.addHandler(handler_file)
    handler_file.setFormatter(LogFormatter('[%(asctime)s][%(funcName)s:%(lineno) 3d][%(levelname)-7s] %(message)s'))

logger.propagate = False

def main(args):
    try:
        #ファイルを読込
        try:
            with open(args.config_file, 'r') as input_file:
                json_data = json.load(input_file)

        except UnicodeDecodeError as ex:
            try:
                logger.warning('UTF-8で読み込みを試みます')
                with open(args.config_file, 'r',encoding="utf-8") as input_file: #UTF-8
                    json_data = json.load(input_file)

            except json.decoder.JSONDecodeError as ex:
                logger.warning('UTF-8(BOM付き)で読み込みを試みます')
                with open(args.config_file, 'r',encoding="utf-8_sig") as input_file: #UTF-8(BOM)
                    json_data = json.load(input_file)
            except Exception as ex:
                # それ以外はraise
                raise ex

        #設定値を取得
        args.exchange   = json_data['exchange']
        #args.ohlc_high  = int(json_data['high']) #廃止(bitflyer cryptowatchのみ利用)
        #args.ohlc_low   = int(json_data['low'])  #廃止(bitflyer cryptowatchのみ利用)
        args.lot        = float(json_data['lot'])
        args.sleep_time = int(json_data['sleep'])
        args.api_key    = json_data['api_key']
        args.api_secret = json_data['api_secret']

        # OHLC 時間足
        args.ohlc_periods = 3600
        if 'ohlc_periods' in json_data and json_data['ohlc_periods'] > 0:
            args.ohlc_periods = json_data['ohlc_periods']

        # OHLC ローソク本数
        args.ohlc_range = 18
        if 'ohlc_range' in json_data and json_data['ohlc_range'] > 0:
            args.ohlc_range = json_data['ohlc_range']

        # Lot ドテン対応
        args.lot_overtake = False
        if 'lot_overtake' in json_data and json_data['lot_overtake'] != 0:
            args.lot_overtake = True

        # 注文有効期間
        args.order_expire_time = 5
        if 'order_expire_time' in json_data and json_data['order_expire_time'] > 0:
            args.order_expire_time = int(json_data['order_expire_time'])

        # 注文リトライ回数(回数x0.5sec)
        args.order_retry_count = 30
        if 'order_retry_count' in json_data and json_data['order_retry_count'] > 0:
            args.order_retry_count = int(json_data['order_retry_count'])

        # 注文待機回数(回数x5sec)
        args.order_wait_count = 60
        if 'order_wait_count' in json_data and json_data['order_wait_count'] > 0:
            args.order_wait_count = int(json_data['order_wait_count'])

        # 価格マージン(high)
        args.margin_high = 0
        if 'margin_high' in json_data:
            args.margin_high = int(json_data['margin_high'])

        # 価格マージン(low)
        args.margin_low = 0
        if 'margin_low' in json_data:
            args.margin_low = int(json_data['margin_low'])

        # LINE Notify(通知)
        args.line_api_token = None
        if 'line_api_token' in json_data and json_data['line_api_token'] != '':
            # 設定にTokenパラメータがある場合は読み込む
            args.line_api_token = json_data['line_api_token']

        # LINE Notify レポート間隔
        args.line_report_interval = 0
        if 'line_report_interval' in json_data and json_data['line_report_interval'] > 0:
            # 設定にTokenパラメータがある場合は読み込む
            args.line_report_interval = int(json_data['line_report_interval'])

        # Bot controller
        args.controller_enable = False
        if 'controller_enable' in json_data and json_data['controller_enable'] != 0:
            args.controller_enable = True

    except FileNotFoundError as ex:
        logger.error('{} 設定ファイルを作成してください'.format(args.config_file))
        exit(1)
    except KeyError as ex:
        logger.error('{}の設定に不足があります'.format(args.config_file))
        exit(1)
    except TypeError as ex:
        logger.error('{}の設定に誤り(数値等)があります'.format(args.config_file))
        exit(1)
    except UnicodeDecodeError as ex:
        logger.error('{}の設定にマルチバイト文字or認識不能ファイル文字コードです'.format(args.config_file))
        exit(1)
    except json.decoder.JSONDecodeError as ex:
        logger.error('{}のJSONフォーマットに誤りor認識不能ファイル文字コードです'.format(args.config_file))
        exit(1)
    except Exception as ex:
        # それ以外はraise
        raise ex

    # Bot controller
    controller = None
    if args.controller_enable is True:
        import bot_controller
        controller = bot_controller.BotController()
        if controller.initialize(args.config_file) is False:
            logger.error('Bot controllerの初期化に失敗しました　設定を確認してください')
            exit(1)

        logger.addHandler(controller.get_logDBHandler())
        controller.bot_status = "START"

    # モジュール読み込み
    excahge_inst = None
    path = os.path.splitext('exchange' + os.path.sep + args.exchange + '.py')[0].replace(os.path.sep, '.')
    try:
        module = importlib.import_module(path)

        excahge_inst = module.Process(args, logger)
    except:
        raise

    logger.info('--- Start Up ---')
    if args.line_api_token is not None:
        line_notify("({:})\nドテン君 ごった煮を起動します".format(args.exchange), args.line_api_token)

    timer_notify = 0
    while True:
        try:
            # Initialize
            message       = None
            error_message = None
            order_id      = None

            if controller is not None and controller.bot_status == 'STOP_ALL':
                error_message = '*全停止状態となっています(Bot controller)*'
                continue

            # OHLC(ローソク足)データ取得
            ohlcv     = excahge_inst.get_ohlc_data(args.ohlc_periods)
            if ohlcv is None:
                # チャート取得失敗
                logger.warning('チャート取得失敗')
                time.sleep(10)
                continue

            # チャンネル計算
            channel       = excahge_inst.calc_channel(ohlcv, args.ohlc_range)
            channel_high  = channel['high'] + args.margin_high
            channel_low   = channel['low'] + args.margin_low

            # 建玉情報取得
            position     = excahge_inst.get_position()

            # 約定データ取得 -> LTP:最終約定価格
            ticker_ltp    = float(excahge_inst.get_ticker()['ltp'])

            ## bitFlyerとBitMEXではコードが違うため無効化
            #logger.debug(str(datetime.datetime.fromtimestamp(ohlcv[-4][0] + 60 * 60 * 8)) + ' :' + str(ohlcv[-4][args.ohlc_high]) + '>' + str(ohlcv[-4][args.ohlc_low]))
            #logger.debug(str(datetime.datetime.fromtimestamp(ohlcv[-3][0] + 60 * 60 * 8)) + ' :' + str(ohlcv[-3][args.ohlc_high]) + '>' + str(ohlcv[-3][args.ohlc_low]))
            #logger.debug(str(datetime.datetime.fromtimestamp(ohlcv[-2][0] + 60 * 60 * 8)) + ' :' + str(ohlcv[-2][args.ohlc_high]) + '>' + str(ohlcv[-2][args.ohlc_low]))

            # indicator
            logger.info('{}'.format(output_indicator(ticker_ltp, channel_high, channel_low, position['side'], float(position['size']))))

            logger.info('{:10}: {:,.0f}'.format('最終価格', ticker_ltp))
            logger.info('{:12}: {:,.0f}'.format('LONG価格', channel_high))
            logger.info('{:12}: {:,.0f}'.format('SHORT価格', channel_low))
            logger.info('{:12}: {:}'.format('方向', str(position['side'])))
            logger.info('{:9}: {:,.4f}'.format('ロット枚数', float(position['size'])))

            profit = 0
            if position['size'] != 0:
                logger.info('{:8}: {:,.0f}'.format('平均約定価格', position['price_avg']))
                profit = (ticker_ltp - position['price_avg'])*position['size']
                logger.info('{:8}: {:,.0f}'.format('トレード損益', profit))

            timer_now = int(time.time())
            if args.line_api_token is not None and args.line_report_interval > 0 and timer_now > timer_notify:
                line_notify("({:})\n最終価格:{:,.0f}\nLONG価格:{:,.0f}\nSHORT価格:{:,.0f}\n方向:{:}\n枚数:{:,.4f}\n価格:{:,.0f}\n損益:{:,.0f}".format(
                    args.exchange, ticker_ltp, channel_high, channel_low, str(position['side']), float(position['size']), position['price_avg'], profit), args.line_api_token)
                timer_notify = int((timer_now + args.line_report_interval)/args.line_report_interval)*args.line_report_interval

            if controller is not None:
                controller.set_info(
                    ticker_ltp,
                    channel_high,
                    channel_low,
                    str(position['side']),
                    float(position['size']),
                    profit)

                if controller.bot_status == 'STOP_ORDER':
                    error_message = '*注文停止状態となっています(Bot controller)*'
                    continue

            if args.dry_run is True:
                error_message = '*注文停止状態となっています(dry-run mode)*'
                continue

            # 注文ロット
            order_lot = args.lot
            # ロットのドテンを有効
            if args.lot_overtake == True:
                order_lot += abs(position['size'])

            if channel_high < ticker_ltp   and (position['side'] == 'SHORT' or position['side'] is None):
                message = "買い注文実行\n LTP  :{:,.0f}\n 枚数 :{}".format(ticker_ltp, order_lot)

                order_id = excahge_inst.send_order(side='BUY', size=order_lot, expire=args.order_expire_time, retry=args.order_retry_count)
                if order_id is None:
                    message = message + "\n" + ' -> 注文に失敗しました'

            elif ticker_ltp   < channel_low and (position['side'] == 'LONG' or position['side'] is None):
                message = "売り注文実行\n LTP  :{:,.0f}\n 枚数 :{}".format(ticker_ltp, order_lot)
                order_id = excahge_inst.send_order(side='SELL', size=order_lot, expire=args.order_expire_time, retry=args.order_retry_count)
                if order_id is None:
                    message = message + "\n" + ' -> 注文に失敗しました'

            # 注文が約定orキャンセルするまで待機
            if order_id is not None:
                result = excahge_inst.wait_for_complete_order(order_id, args.order_wait_count)
                if result is not None:
                    message = message + "\n" + ' -> 注文が完了しました'
                else:
                    message = message + "\n" + ' -> 注文処理 待機時間({}s)を超過しました 次のループに移ります'.format(args.order_wait_count)

            if message is not None:
                logger.info(message.replace("\n", ""))
                if args.line_api_token is not None:
                    line_notify("({:})\n{:}".format(args.exchange, message), args.line_api_token)

            # 次の処理まで待機
            logger.debug('wait a while ({} sec)'.format(args.sleep_time))
            time.sleep(args.sleep_time)

        #以下は例外処理を受信するための処理
        except KeyboardInterrupt as ex:
            logger.warning('入力により割り込み終了しました')
            return
        except TypeError  as ex:
            error_message = 'TypeError, {}'.format(str(ex))
            print(traceback.format_exc())
        except KeyError  as ex:
            error_message = 'KeyError, {}'.format(str(ex))
            print(traceback.format_exc())
        except ValueError  as ex:
            error_message = 'ValueError, {}'.format(str(ex))
            print(traceback.format_exc())
        except HTTPError  as ex:
            error_message = 'HTTPError, {}'.format(str(ex))
            print(traceback.format_exc())
        except ConnectionError  as ex:
            error_message = 'requests ConnectionError, {}'.format(str(ex))
            print(traceback.format_exc())
        except HTTPError  as ex:
            error_message = 'requests HTTPError, {}'.format(str(ex))
            print(traceback.format_exc())
        except Exception as ex:
            # それ以外はraise
            logger.critical(ex)
            raise ex
        finally:
            if error_message is not None:
                logger.warning(error_message)
                logger.debug('wait a while ({} sec)'.format(args.sleep_time))
                time.sleep(args.sleep_time)

if __name__ == '__main__':
    main(args)
