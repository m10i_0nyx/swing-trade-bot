# -*- coding: utf-8 -*-

# Copyright (c) 2018 m10i <m10i@0nyx.net>
#
#----------------------------------------------------------
# *** 利用ライセンスについて ***
# 下記の条件を受け入れていただけるのであれば、誰でも自由に無料で、このソフトウェアをつかっていただくことができます
# ・このソースコードには一切の保証は有りません
# ・このソースコードを利用したことで何らかの問題・損害を負ったとしても、作者はなんの責任も負いません
# ・ソースコードを自身の利用範囲でコピーしたり、変更を加えるのは自由です
# ・ただし作者の許可無しでの第3者への再配布および販売は固く禁じます
# ・この条件を逸脱した場合は作者(もしくは関係者)より利用禁止、または損害賠償を要求いたします
#----------------------------------------------------------

import requests
from requests.exceptions import ConnectionError, HTTPError
import json
import sys
import time
import ccxt
from ccxt.base.errors import ExchangeError, NetworkError, InvalidOrder

# BitMEX用 Class
class Process:
    # クラス初期
    def __init__(self, args, logger):

        # インスタンス初期値
        self.args   = args
        self.logger = logger

        # API初期化
        self.api    = ccxt.bitmex({
           'apiKey': args.api_key,
           'secret': args.api_secret
        })

        self.product_code = 'BTC/USD'

    # OHLCデータ取得
    def get_ohlc_data(self, periods, retry=3, length=100):
        market_name='XBTUSD'
        now = int(time.time()) # 現在時刻の取得
        for idx in range(retry):
            response      = requests.get('https://www.bitmex.com/api/udf/history?symbol={}&resolution={}&from={}&to={}'.format(
                market_name,
                periods/60,
                int(now) - periods * length,
                int(now)
                ))
            if response is not None:
                break

        return response.json()

    # 約定情報取得
    def get_ticker(self, retry=5):
        ohlcv = self.get_ohlc_data(60, retry=retry, length=0)
        return {'ltp': ohlcv['c'][0]}

    # ポジション取得
    def get_position(self, retry=5):
        product_code='XBTUSD'
        for idx in range(retry):
            try:
                response = self.api.private_get_position({'filter': json.dumps({"symbol": product_code})})
                #self.logger.debug(response)
                if response is not None:
                    break
            except ExchangeError as ex:
                self.logger.warning(ex)
            except InvalidOrder as ex:
                self.logger.warning(ex)
            except NetworkError as ex:
                self.logger.warning(ex)
                time.sleep(10)

        if response is None or len(response) == 0:
            return {'side': None, 'size': 0.0, 'price_avg': 0}

        pos = response[0]

        #over write
        if pos['avgEntryPrice'] is None:
            pos['avgEntryPrice'] = 0

        if pos['currentQty'] == 0: # sizeが0より大きければ現在LONG状態、0より小さければ現在SHORT状態と判断
            side = None
        elif pos['currentQty'] > 0:
            side = 'LONG'
        else:
            side = 'SHORT'
        return {'side': side, 'size': round(pos['currentQty']), 'price_avg': pos['avgEntryPrice']}

    # 注文送信
    def send_order(self, side, size, expire = 2, retry = 32, order_type='market'):
        product_code=self.product_code
        self.logger.debug('Order, side:{:5}, size:{:,.4f}'.format(side, size))

        response = None
        for idx in range(retry):
            try:
                response = self.api.create_order(product_code, type=order_type, side=side.lower(), amount=size)
                #self.logger.debug(response)
                if response is not None:
                    break
            except ExchangeError as ex:
                self.logger.warning(ex)
            except InvalidOrder as ex:
                self.logger.warning(ex)
            except NetworkError as ex:
                self.logger.warning(ex)
                time.sleep(10)

            time.sleep(5)

        if response is not None and 'id' in response:
            return response['id'] #注文受領IDを返す
        else:
            return None

    # 注文処理待ち
    def wait_for_complete_order(self, child_order_acceptance_id, count = 60):
        product_code=self.product_code
        for idx in range(count):
            try:
                response = self.api.private_get_position()

                #self.logger.debug(response)
                if response is not None and len(response) != 0:
                    return True
            except ExchangeError as ex:
                self.logger.warning(ex)
            except InvalidOrder as ex:
                self.logger.warning(ex)
            except NetworkError as ex:
                self.logger.warning(ex)
                time.sleep(10)

            time.sleep(5) # 未完了

        return None

    # チャンネル計算
    def calc_channel(self, ohlcv, channel_period, high='h', low='l'):
        channel_high = 0
        channel_low  = sys.maxsize

        for i in range(channel_period): # [-2]から[-(2-period)]までの最高値・最安値を計算([-1]が最新1時間足)
            if ohlcv[high][-i-2] > channel_high:
                channel_high = ohlcv[high][-i-2]

            if(ohlcv[low][-i-2] < channel_low):
                channel_low = ohlcv[low][-i-2]

        return {'high': channel_high, 'low': channel_low}
