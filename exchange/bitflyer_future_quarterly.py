# -*- coding: utf-8 -*-

# Copyright (c) 2018 m10i <m10i@0nyx.net>
#
#----------------------------------------------------------
# *** 利用ライセンスについて ***
# 下記の条件を受け入れていただけるのであれば、誰でも自由に無料で、このソフトウェアをつかっていただくことができます
# ・このソースコードには一切の保証は有りません
# ・このソースコードを利用したことで何らかの問題・損害を負ったとしても、作者はなんの責任も負いません
# ・ソースコードを自身の利用範囲でコピーしたり、変更を加えるのは自由です
# ・ただし作者の許可無しでの第3者への再配布および販売は固く禁じます
# ・この条件を逸脱した場合は作者(もしくは関係者)より利用禁止、または損害賠償を要求いたします
#----------------------------------------------------------

import requests
import json
import sys
import time
import pybitflyer

# bitFlyer用 Class
class Process:
    # クラス初期
    def __init__(self, args, logger):

        # インスタンス初期値
        self.args   = args
        self.logger = logger

        # API初期化
        self.api    = pybitflyer.API(api_key=args.api_key, api_secret=args.api_secret)

        # マーケット product code取得
        # 四半期はalias名が固定だが、product codeは都度変更となるため探す必要がある
        self.product_code = None
        response      = requests.get('https://api.bitflyer.jp/v1/getmarkets')

        for board in response.json():
            if 'alias' in board and board['alias'] == 'BTCJPY_MAT3M':
                self.product_code = board['product_code']
                break

        if self.product_code is None:
            logger.critical("先物四半期 alias:BTCJPY_MAT3M のproduct_codeが見つかりませんでした")
            raise Exception('Market product code not found')

    # OHLCデータ取得
    def get_ohlc_data(self, periods, retry=3):
        market_name='btcjpy-quarterly-futures'
        for idx in range(retry):
            response      = requests.get('https://api.cryptowat.ch/markets/bitflyer/{}/ohlc?periods={}'.format(market_name, periods))
            if response is not None:
                break

        ohlc_data = response.json()

        if 'result' in ohlc_data:
            return ohlc_data['result'][str(periods)]
        else:
            # チャート取得失敗
            self.logger.debug(ohlc_data)
            return None

    # 約定情報取得
    def get_ticker(self, retry=3):
        product_code=self.product_code
        for idx in range(retry):
            ticker = self.api.ticker(product_code=product_code)
            if ticker is not None:
                break

        ticker['last'] = ticker['ltp'] #BitMEX共通化
        return ticker

    # ポジション取得
    def get_position(self, retry=3):
        product_code=self.product_code
        for idx in range(retry):
            response = self.api.getpositions(product_code=product_code)
            #self.logger.debug(response)
            if response is not None:
                break

        if 'error_message' in response:
            if response['status'] == -500:
                # ACCESS-KEY header is required
                raise KeyError(response.get('error_message'))
            elif 'status' in response:
                raise Exception('status code:' + str(response['status']) + '/' + response.get('error_message'))
            else:
                raise Exception(response.get('error_message'))

        size = 0.0 # float
        evaluation_sum = 0.0
        size_sum = 0.0
        for posision in response:
            try:
                evaluation_sum += posision['price'] * posision['size']
                size_sum += posision['size']
                if posision['side'] == 'BUY':
                    size += posision['size']

                if posision['side'] == 'SELL':
                    size -= posision['size']

            except TypeError as ex:
                self.logger.debug(posision)
                self.logger.warning(str(ex))

        if size > 0:
            side = 'LONG' #BUY
        elif size < 0:
            side = 'SHORT' #SELL
        else:
            side = None  #no posission

        price_avg = 0
        if evaluation_sum > 0 and size_sum > 0:
            price_avg = evaluation_sum / size_sum

        return {'side':side, 'size':size, 'price_avg':price_avg}

    # 注文送信
    def send_order(self, side, size, expire = 2, retry = 32, order_type='MARKET'):
        product_code=self.product_code
        self.logger.debug('Order, side:{:5}, size:{:,.4f}'.format(side, size))

        response = None
        for idx in range(retry):
            response = self.api.sendchildorder(product_code=product_code, child_order_type=order_type, minute_to_expire = expire, side = side.upper(), size = size)
            self.logger.debug(response)

            if 'error_message' in response:
                if response['status'] == -104:
                    # status code:-104/Invalid order amount
                    self.logger.warning(response.get('error_message'))
                    size = round(size, 2)
                    time.sleep(0.5)
                    continue
                elif response['status'] == -205:
                    # Margin amount is insufficient for this order.
                    self.logger.warning(response.get('error_message'))
                    if size > 0.01:
                        size = size / 2 #半分にしてリトライ
                    time.sleep(0.5)
                    continue
                elif response['status'] == -208:
                    # Order is not accepted. Please try again later
                    self.logger.warning(response.get('error_message'))
                    time.sleep(0.5)
                    continue
                elif response['status'] == -500:
                    # ACCESS-KEY header is required
                    self.logger.warning(response.get('error_message'))
                    time.sleep(2)
                    continue
                elif 'status' in response:
                    raise Exception('status code:' + str(response['status']) + '/' + response.get('error_message'))
                else:
                    raise Exception(response.get('error_message'))
            else:
                break

        if 'child_order_acceptance_id' in response:
            return response['child_order_acceptance_id'] #注文受領IDを返す
        else:
            return None #想定外にない場合

    # 注文処理待ち
    def wait_for_complete_order(self, child_order_acceptance_id, count = 60):
        product_code=self.product_code
        for idx in range(count):
            response = self.api.getchildorders(product_code=product_code, child_order_acceptance_id=child_order_acceptance_id, count=1)
            #self.logger.debug(response)

            if 'error_message' in response:
                self.logger.debug(response)
                self.logger.warning(response.get('error_message'))
                time.sleep(5)
                continue

            elif len(response) >= 1 and response[0]['child_order_state'] != 'ACTIVE':
                self.logger.debug(response)
                return response[0] #注文完了 or キャンセル等

            else:
                time.sleep(5) # ACTIVE or 未完了
                continue

        return None

    # チャンネル計算
    def calc_channel(self, ohlcv, channel_period, high=2, low=3):
        channel_high = 0
        channel_low  = sys.maxsize

        for i in range(channel_period): # [-2]から[-(2-period)]までの最高値・最安値を計算([-1]が最新1時間足)
            if ohlcv[-i-2][high] > channel_high:
                channel_high = ohlcv[-i-2][high]

            if(ohlcv[-i-2][low] < channel_low):
                channel_low = ohlcv[-i-2][low]

        return {'high': channel_high, 'low': channel_low}
