#!/usr/bin/python3
'''
1.pubnubが未導入の場合はsudo pip-3.6 install pubnubでインストールしてください
2.bot_controller_sample.pyを参考にコードをBotに埋め込んでください
　サンプルには公式バージョン1.0.1へ組み込む場合の行数を入れています
　BitMexでも動作し、情報の確認はできるのですが、情報確認画面がbitFlyer用になっていますので、
　円表記等、少し表示に違いがあります
3.pubnubのキーを発行してください(「pubnubのアカウント作成からキー発行まで」を参照)
4.発行したキーをbc_setting.jsonへ入力してください
　併せて情報表示画面に表示するBotの名前を設定してください
5.組み込んだBotを実行してください
　正常に動作が開始されると情報が発信され始めます
6.http://ccweb.sakura.ne.jp/dotenkun/bf.htmlへアクセス、
　pubnubのキーを入力し、保存ボタンを押します
7.正しく情報が表示されれば成功です

pubnubのアカウント作成からキー発行まで
1.pubnubのページ(https://www.pubnub.com)を開き、右上のGet Startedを押し、
　アカウント作成ページから作成してください
2.作成完了後、personal's appsページの右上のCREATE NEW APPボタンを押し、名前を入力し、
　CREATEボタンを押します
3.作成されたAPPをクリックします
4.Demo Keysetをクリックします
5.Publish Key、Subscribe Key、Secret Keyが表示されます
'''

import logging
import logging.handlers
from logging import getLogger, Formatter as LogFormatter, StreamHandler as LogStreamHandler, DEBUG as LOG_DEBUG, INFO as LOG_INFO
from pubnub.callbacks import SubscribeCallback
from pubnub.enums import PNStatusCategory
from pubnub.pnconfiguration import PNConfiguration
from pubnub.pubnub import PubNub
from datetime import datetime,timedelta
from pytz import timezone, utc
import os
import time
import json
import sqlite3
from contextlib import closing

class BotController(SubscribeCallback):

    _VERSION = 0

    _CHANNEL = 'control'

    @property
    def bot_status(self):
        return self._bot_status

    @bot_status.setter
    def bot_status(self, value):
        self._logger.info('statusChanged status:' + value)
        self._bot_status = value
        self.__publish_command_data('get_status_res', {'name':self._name, 'status':self._bot_status})

    #コンストラクタ
    def __init__(self):
        self._initialized = False
        self._buffer_count = 20
        self._bot_status = 'STOP_ALL'
        self._info_time = datetime.now()
        self._last = 0
        self._upper = 0
        self._lower = 0
        self._pos_side = ""
        self._pos_pnl = 0
        self._pos_size = 0


    #初期化処理
    def initialize(self, config_file):
        self._logger = logging.getLogger('bot_controller')
        self._logger.setLevel(10)
        #fh = logging.FileHandler('bot_controller.log')
        if not os.path.exists('./logs'):
            os.mkdir('./logs')
        fh = logging.handlers.TimedRotatingFileHandler(
            filename = 'logs/bot_controller.log',
            when = 'D',
            backupCount=60,
            encoding='utf-8'
        )
        self._logger.addHandler(fh)
        #sh = logging.StreamHandler()
        #self._logger.addHandler(sh)
        formatter = logging.Formatter(
            '%(asctime)s:%(lineno)d: %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
        fh.setFormatter(formatter)
        #sh.setFormatter(formatter)
        logging.Formatter.converter = self.__customTime

        if (self.__load_setting(config_file) == False):
            return False
        if (self.__init_db() == False):
            return False
        return True

    #設定データ読み込み
    def __load_setting(self, config_file):
        try:
            self._logger.info('初期化開始')

            try:
                with open(config_file, 'r') as input_file:
                    json_data = json.load(input_file)

            except UnicodeDecodeError:
                try:
                    self._logger.warning('UTF-8で読み込みを試みます')
                    with open(config_file, 'r',encoding="utf-8") as input_file:
                        json_data = json.load(input_file)

                except json.decoder.JSONDecodeError:
                    self._logger.logger.warning('UTF-8(BOM付き)で読み込みを試みます')
                    with open(config_file, 'r',encoding="utf-8_sig") as input_file:
                        json_data = json.load(input_file)
                except Exception as ex:
                    self._logger.warning(ex)
                    return False
            except Exception as ex:
                self._logger.warning(ex)
                return False

            self._name = json_data['controller_name']
            self._message_days = json_data['controller_message_days']
            self._dbname = 'datas/bot_controller.db'
            pnconfig = PNConfiguration()
            pnconfig.publish_key = json_data['controller_pub_key']
            pnconfig.subscribe_key = json_data['controller_pub_sub_key']
            pnconfig.secret_key = json_data['controller_pub_sec_key']
            self._pubnub = PubNub(pnconfig)
            self._pubnub.add_listener(self)
            self._pubnub.subscribe().channels(self._CHANNEL).execute()
            self._initialized = True

            self._logger.info('初期化終了')
            return True
        except Exception as ex:
            self._logger.warning(ex)
            return False

    #DB初期化
    def __init_db(self):
        try:
            if not os.path.exists('./datas'):
                os.mkdir('./datas')

            with closing(sqlite3.connect(self._dbname, timeout=1)) as connection:
                cursor = connection.cursor()
                sql = "CREATE TABLE IF NOT EXISTS message (id INTEGER PRIMARY KEY AUTOINCREMENT, level INTEGER, time INTEGER, message varchar(100))"
                cursor.execute(sql)

                sql = "SELECT * FROM sqlite_master WHERE type = 'index' AND name = 'index_message_time'"
                rows = cursor.execute(sql)
                if(len(rows.fetchall()) <= 0):
                    sql = "CREATE INDEX index_message_time ON message(level, time)"
                    cursor.execute(sql)

                connection.close()
            return True
        except Exception as ex:
            self._logger.warning(ex)
            return False

    #メッセージ配信
    def __publish_message(self, message):
        try:
            if self._initialized == False:
                return

            self._pubnub.publish().channel(self._CHANNEL).message(message).async(self.__publish_callback)
        except Exception as ex:
            self._logger.warning(ex)

    #メッセージ配信
    def __publish_command_data(self, command, data):
        try:
            if self._initialized == False:
                return

            pack = {'version':self._VERSION, 'command': command, 'data': data}
            self._pubnub.publish().channel(self._CHANNEL).message(pack).async(self.__publish_callback)
        except Exception as ex:
            self._logger.warning(ex)


    #メッセージ受信
    def message(self, pubnub, message):
        try:
            if (message.channel!=self._CHANNEL):
                return

            if (message.message['command']=='get_status_req'):
                #ステータス取得リクエスト
                self._logger.info('get_status_req name:' + self._name + ' status:' + self._bot_status)
                self.__publish_command_data('get_status_res', {'name':self._name, 'status':self._bot_status})
            elif (message.message['command']=='set_status_req'):
                #ステータス設定リクエスト
                self._logger.info('set_status_req status:' + message.message['data'])
                self.bot_status = message.message['data']
            elif (message.message['command']=='get_info_req'):
                #情報リクエスト
                self._logger.info('get_info_req')
                self.__send_info()
            elif (message.message['command']=='get_message_req'):
                #メッセージ・リクエスト
                self._logger.info('get_message_req count:{}'.format(message.message['data']))
                self.__send_buffered_message(message.message['data']['level'], message.message['data']['count'])
        except Exception as ex:
            self._logger.warning(ex)


    #情報設定
    def set_info(self, last, upper, lower, pos_side, pos_size, pos_pnl):
        try:
            self._info_time = datetime.now()
            self._last = last
            self._upper = upper
            self._lower = lower
            self._pos_side = pos_side
            self._pos_size = pos_size
            self._pos_pnl = pos_pnl
            self.__send_info()
        except Exception as ex:
            self._logger.warning(ex)


    #情報配信
    def __send_info(self):
        try:
            response = {
                'time':self.__getUnixTime(self._info_time),
                'last':self._last, 'upper':self._upper, 'lower':self._lower,
                'side':self._pos_side, 'size':self._pos_size, 'pnl':self._pos_pnl}
            self.__publish_command_data('get_info_res', response)
        except Exception as ex:
            self._logger.warning(ex)


    #バッファメッセージ配信
    def __send_buffered_message(self, level, count):
        try:
            with closing(sqlite3.connect(self._dbname, timeout=1)) as connection:
                connection.row_factory = sqlite3.Row
                cursor = connection.cursor()
                sql = 'SELECT * FROM message WHERE level>={} ORDER BY time DESC LIMIT {}'.format(level, count)
                rows = cursor.execute(sql).fetchall()
                connection.close()

                for row in reversed(rows):
                    pack = {
                        'version':self._VERSION, 'command' : 'get_message_res',
                        'data' : {'time' : int(row['time']), 'level' : int(row['level']), 'message' : str(row['message'])}}
                    self.__publish_message(pack)
        except Exception as ex:
            self._logger.warning(ex)


    #メッセージ配信
    def send_message(self, time, level, data):
        try:
            pack = {'version':self._VERSION, 'command' : 'push_message', 'data' : {'time' : time, 'level' : level, 'message' : data}}
            self.__publish_message(pack)
        except Exception as ex:
            self._logger.warning(ex)

    def send_order_info(self, side, size):
        try:
            self.__publish_command_data('order', {'side' : side, 'size' : size})
        except Exception as ex:
            self._logger.warning(ex)

    def get_logDBHandler(self):
        connection = sqlite3.connect(self._dbname, timeout=1)
        cursor = connection.cursor()
        handler = LogDBHandler(self._logger, self, connection, cursor, self._message_days)
        return handler


    def __customTime(self, *args):
        utc_dt = utc.localize(datetime.utcnow())
        my_tz = timezone("Japan")
        converted = utc_dt.astimezone(my_tz)
        return converted.timetuple()


    def presence(self, pubnub, presence):
        pass


    def status(self, pubnub, status):
        if status.category == PNStatusCategory.PNUnexpectedDisconnectCategory:
            self._logger.debug('PNUnexpectedDisconnectCategory')
        elif status.category == PNStatusCategory.PNConnectedCategory:
            self._logger.debug('PNConnectedCategory')
        elif status.category == PNStatusCategory.PNReconnectedCategory:
            self._logger.debug('PNReconnectedCategory')
        elif status.category == PNStatusCategory.PNDecryptionErrorCategory:
            self._logger.debug('PNDecryptionErrorCategory')


    def __publish_callback(self, envelope, status):
        if not status.is_error():
            pass
        else:
            pass


    def __getUnixTime(self, dt):
        return int(time.mktime(dt.timetuple()))


#ログDB書き込みクラス
class LogDBHandler(logging.Handler):
    def __init__(self, logger, controller, connection, cursor, max_days):
        logging.Handler.__init__(self)
        self._logger = logger
        self._controller = controller
        self._cursor = cursor
        self._connection = connection
        self._max_days = max_days


    def emit(self, record):
        try:
            now = datetime.now()
            time = self.__getUnixTime(now)
            level = record.levelno
            message = str(record.msg)

            self._controller.send_message(time, level, message)

            if (len(message) > 100):
                message = message[:100]
            sql = 'INSERT INTO message (level, time, message) values (?,?,?)'
            message = (record.levelno, time, message)

            self._cursor.execute(sql, message)
            self._connection.commit()

            time = self.__getUnixTime(now - timedelta(days=self._max_days))
            sql = 'DELETE FROM message WHERE time < {}'.format(time)

            self._cursor.execute(sql)
            self._connection.commit()
        except Exception as ex:
            self._logger.warning(ex)


    def __getUnixTime(self, dt):
        return int(time.mktime(dt.timetuple()))
